import os
import repoutil as repo


def test_version():
    assert repo.__version__ == '0.1.0'

def test_get_username():
    assert repo.get_username() == "Shravan Asati\n"

def test_get_year():
    assert repo.get_year() == "2021"

def test_get_gitignore():
    gitignore_dir = os.path.join("repoutil", "gitignores")
    languages = [i.split(".")[0] for i in os.listdir(gitignore_dir)]
    
    for lang in languages:
        with open(os.path.join(gitignore_dir, lang + ".gitignore"), "r") as f:
            assert f.read() == repo.get_gitignore(lang)

    assert repo.get_gitignore("this language doesnt exists") == ""


def test_get_license():
    license_dir = os.path.join("repoutil", "licenses")
    licenses = [i for i in os.listdir(license_dir)]
    
    for licens in licenses:
        with open(os.path.join(license_dir, licens), "r", encoding="utf-8") as f:
            file_content = f.read().replace(":NAME:", repo.get_username()).replace(":YEAR:", repo.get_year())
            assert file_content == repo.get_license(licens)

    assert repo.get_license("this license doesnt exists") == ""


def test_get_workflow():
    workflow_dir = os.path.join("repoutil", "workflows")
    workflows = [i.split(".")[0] for i in os.listdir(workflow_dir)]
    
    for workflow in workflows:
        with open(os.path.join(workflow_dir, workflow + ".yml"), "r") as f:
            file_content = f.read()
            assert file_content == repo.get_workflow(workflow)

    assert repo.get_license("this license doesnt exists") == ""